import { VirtualImagesMapper } from './../../mappers/vm-images/virtual-images-mapper';
import { VirtualImagesDTO } from './../../DTOs/vm-images/virtual-images-dto';
import { Observable } from 'rxjs';
import { KypoSandboxApiConfigService } from '../../others/kypo-sandbox-api-config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VirtualImage } from '@muni-kypo-crp/sandbox-model';
import { SentinelParamsMerger } from '@sentinel/common';
import { PaginatedResource, OffsetPaginationEvent } from '@sentinel/common/pagination';
import { SentinelFilter } from '@sentinel/common/filter';
import { PaginationParams } from '../../http/pagination-params';
import { PaginationMapper } from '../../mappers/pagination-mapper';
import { DjangoResourceDTO } from '../../DTOs/other/django-resource-dto';
import { VMImagesApi } from './vm-images-api.service';
import { map } from 'rxjs/operators';
import { FilterParams } from '../../http/filter-params';

/**
 * Default implementation of service abstracting http communication with vm images endpoints.
 */
@Injectable()
export class VMImagesDefaultApi extends VMImagesApi {
  private readonly virtualImagesUriExtension = 'images';
  private readonly virtualImagesEndpointUri = this.context.config.sandboxRestBasePath + this.virtualImagesUriExtension;

  constructor(private http: HttpClient, private context: KypoSandboxApiConfigService) {
    super();
    if (this.context.config === undefined || this.context.config === null) {
      throw new Error(
        'KypoSandboxApiConfig is null or undefined. Please provide it in forRoot() method of KypoSandboxApiModule' +
          ' or provide own implementation of API services'
      );
    }
  }

  /**
   * Sends http request to retrieve all available virtual machine images
   * @param pagination requested pagination
   * @param onlyKypoImages filters images belonging to KYPO
   * @param onlyGuiAccess filters images with GUI access
   * @param cached Performs the faster version of this endpoint but does not retrieve a fresh list of images
   * @param filters list of sentinel filters to filter results
   */
  getAvailableImages(
    pagination: OffsetPaginationEvent,
    onlyKypoImages = false,
    onlyGuiAccess = false,
    cached = false,
    filters?: SentinelFilter[]
  ): Observable<PaginatedResource<VirtualImage>> {
    const params = SentinelParamsMerger.merge([PaginationParams.create(pagination), FilterParams.create(filters)])
      .append('munikypo', onlyKypoImages)
      .append('GUI', onlyGuiAccess)
      .append('cached', cached);

    return this.http
      .get<DjangoResourceDTO<VirtualImagesDTO>>(`${this.virtualImagesEndpointUri}`, {
        params: params,
      })
      .pipe(
        map(
          (response) =>
            new PaginatedResource<VirtualImage>(
              VirtualImagesMapper.fromDTOs(response.results),
              PaginationMapper.fromDjangoAPI(response)
            )
        )
      );
  }
}
