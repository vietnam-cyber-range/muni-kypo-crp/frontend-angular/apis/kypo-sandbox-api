export class PoolUpdateDTO {
  comment?: string;
  visible: boolean;
  send_emails: boolean;
}
