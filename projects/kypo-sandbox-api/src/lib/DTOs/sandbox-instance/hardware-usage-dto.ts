export class HardwareUsageDTO {
  instances: number;
  network: number;
  port: number;
  ram: number;
  subnet: number;
  vcpu: number;
}
