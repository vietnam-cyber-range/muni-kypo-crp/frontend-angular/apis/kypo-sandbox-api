export class PoolCreateDTO {
  comment: string;
  visible: boolean;
  send_emails: boolean;
  definition_id: number;
  max_size: number;
}
