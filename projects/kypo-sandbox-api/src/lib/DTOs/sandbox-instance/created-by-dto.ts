export class CreatedByDTO {
  full_name: string;
  id: number;
  mail: string;
  sub: string;
}
