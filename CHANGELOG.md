### 16.0.3 Add option for email notifications during sandbox building.
* 27b3976 -- [CI/CD] Update packages.json version based on GitLab tag.
* e0fb2d8 -- Merge branch '54-enable-email-notifications-upon-sandbox-building' into 'master'
* 31b747e -- Add option for email notifications during sandbox building
### 16.0.2 Add an option to force delete a pool.
* 85e793a -- [CI/CD] Update packages.json version based on GitLab tag.
* 781f481 -- Merge branch '53-enable-force-delete-call-for-pools-with-allocations' into 'master'
* 78008a8 -- Add a param for pool force delete
### 16.0.1 Fix allocation api update call.
* 295b568 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9db4743 -- Fix allocation api update call
### 16.0.0 Update to Angular 16, update sandbox pool and allocation units calls.
* 15255ed -- [CI/CD] Update packages.json version based on GitLab tag.
* 1476e2f -- Merge branch '50-update-to-angular-16-2' into 'master'
* 1e8bbb2 -- Update to Angular 16 and update sandbox model's pool and allocation units.
### 15.1.6 Fix pool update call to provide comments.
* 0e22a0f -- [CI/CD] Update packages.json version based on GitLab tag.
* f4b176e -- Fix pool update call to provide comments
### 15.1.5 Adjust pool and sandbox update calls.
* c3aa3af -- [CI/CD] Update packages.json version based on GitLab tag.
* 55d7447 -- Merge branch '52-adjust-pool-and-sandbox-update-calls' into 'master'
* ab3bb23 -- Adjust pool and sandbox update calls
### 15.1.4 Add edition for pools, introduce comments for pools and their sandboxes.
* e35e988 -- [CI/CD] Update packages.json version based on GitLab tag.
* fb8dcc0 -- Merge branch '51-add-calls-to-manage-comments-for-pools-and-sandboxes' into 'master'
* 972354a -- Enable comments to sandboxes and pools
### 15.1.3 Sync allocation call with new paginated results.
* c1811fe -- [CI/CD] Update packages.json version based on GitLab tag.
* 149b3a9 -- Sync allocation calls with sandbox agenda
### 15.1.2 Add option for empty allocation unit array upon allocation call repsonse.
* 8d0b7e3 -- [CI/CD] Update packages.json version based on GitLab tag.
* b3a4652 -- Add option for empty allocation unit array upon allocation call repsonse
### 15.1.1 Enable empty allocation array upon pool overview allocation call.
* b0cf396 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6088eb1 -- Merge branch '49-enable-empty-array-of-alocation-objects-from-sandbox-service' into 'master'
* 885300b -- Enable empty allocation array upon pool overview allocation call
### 15.1.0 Add option to accept empty allocation details from sandbox service's allocation call.
* d7126cf -- [CI/CD] Update packages.json version based on GitLab tag.
* 7a17c37 -- Merge branch '48-adjust-mapping-of-requests-to-handle-empty-allocation-info-for-new-sandboxes' into 'master'
* 4f0c7d3 -- Enable empty allocation details in pool agenda
### 15.0.0 Update project to Angular 15.
* 6765078 -- [CI/CD] Update packages.json version based on GitLab tag.
* e1099d3 -- Initiate build
*   5d0cacb -- Merge branch '46-update-to-angular-15' into 'master'
|\  
| * cefc779 -- Update to Angular 15
|/  
* 2b4d372 -- Merge branch 'develop' into 'master'
* 175101a -- Merge branch '45-modify-network-and-port-display-names' into 'develop'
* eb26129 -- Resolve "Modify network and port display names"
### 14.4.5 Fix display names for ports and networks.
* 6e62a56 -- [CI/CD] Update packages.json version based on GitLab tag.
* 3adfe6f -- Merge branch 'develop' into 'master'
* 4ac1d39 -- Merge develop into master
### 14.4.4 Fix sandbox allocation unit endpoint sorting by id.
* b491146 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9f7cb2e -- Merge branch 'develop' into 'master'
* f2d5a6d -- Updated sandbox allocation unit sorting by id.
### 14.4.3 Fix appending of string prefix in sandboxes endpoint.
* 96a486e -- [CI/CD] Update packages.json version based on GitLab tag.
* 847769f -- Merge branch '44-when-allocating-multiple-sandboxes-clicking-on-show-10-more-button-throws-an-error' into 'master'
* 3e1ce9b -- Updated version.
* 6af669c -- Fixed the fix of sandbox instance sorting.
### 14.4.2 Fix sorting of sandboxes endpoint.
* 0942d1d -- [CI/CD] Update packages.json version based on GitLab tag.
* cdad898 -- Merge branch 'fix-sorting' into 'master'
* 96056db -- Fix sorting
### 14.4.1 Add force to cleanup request.
* e621637 -- [CI/CD] Update packages.json version based on GitLab tag.
* dbdc80e -- Merge branch 'add-force-to-cleanup' into 'master'
* 8d56218 -- Add force to cleanup
### 14.4.0 Change endpoints dealing with sandbox id to use uuid instead.
* 489ed8e -- [CI/CD] Update packages.json version based on GitLab tag.
* 9a44aaf -- Merge branch '42-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
* cad61ac -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
### 14.3.0 Add owner specified information to virtual image. Add query parameter 'GUI' to endpoint to filter images with GUI access.
* 26c17f5 -- [CI/CD] Update packages.json version based on GitLab tag.
* c838517 -- Merge branch '41-refactor-resources-page' into 'master'
* b3844fb -- Resolve "Refactor resources page"
### 14.2.0 Address changes in endpoints for allocation units cleanup. Add cleanup of unlocked and failed allocation units,
* a29cc5d -- [CI/CD] Update packages.json version based on GitLab tag.
* bef4689 -- Merge branch '43-address-cleanup-multiple-endpoint-change' into 'master'
* b49a3da -- Resolve "Address cleanup multiple endpoint change"
### 14.1.0 Use allocation unit id for un/locking sandbox instance. Add definition info to pool dto.
* 8c77ba6 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6f89d3c -- Merge branch '39-optimize-sandbox-agenda' into 'master'
* c5f3e29 -- Resolve "Optimize sandbox agenda"
### 14.0.0 Update to Angular 14.
* a73924e -- [CI/CD] Update packages.json version based on GitLab tag.
* 74638f5 -- Merge branch '38-update-to-angular-14' into 'master'
* 034038c -- Resolve "Update to Angular 14"
### 13.1.1 Add filter for images.
* 76553d5 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5a240c4 -- Merge branch '37-improve-the-images-list-in-the-resources-page' into 'master'
* 517fcfb -- Resolve "Improve the images list in the resources page"
### 13.1.0 Replace openstack endpoints with terraform
* 7310312 -- [CI/CD] Update packages.json version based on GitLab tag.
* 85f1b3f -- Merge branch '36-replace-openstack-with-terraform' into 'master'
* f14aa2c -- Resolve "Replace openstack with terraform"
### 13.0.0 Update to Angular 13 and CI update
* 9096319 -- [CI/CD] Update packages.json version based on GitLab tag.
*   195764c -- Merge branch '35-update-to-angular-13' into 'master'
|\  
| * dca970a -- Resolve "Update to Angular 13"
|/  
* 43dec06 -- Merge branch '34-add-method-for-restart-of-allocation-stages' into 'master'
* 00f341b -- Resolve "Add method for restart of allocation stages"
### 12.0.5 Add support for backend sort
* 4802ac7 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4238fe2 -- Merge branch '33-add-support-for-backend-sorting' into 'master'
* b58f6df -- Resolve "Add support for backend sorting"
### 12.0.4 Add created by field to sandbox definition
* 22223d8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   554dcfb -- Merge branch '32-add-created-by-field-to-sd' into 'master'
|\  
| * 3cd75e1 -- Add created by field
|/  
* f5dc3dd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 299e3d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   11b196f -- Merge branch '29-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 8fb2c17 -- Resolve "Add stage state to allocation unit"
|/  
*   a7fd08e -- Merge branch '31-add-license-file' into 'master'
|\  
| * 5669499 -- Add license file
|/  
*   2934609 -- Merge branch '30-add-node-modules-to-gitignore' into 'master'
|\  
| * 364c137 -- Add node_module to gitignore
|/  
* 5ce6c95 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 383e13f -- [CI/CD] Update packages.json version based on GitLab tag.
*   fc834af -- Merge branch '445537-master-patch-86314' into 'master'
|\  
| * 75eb6a1 -- Update VERSION.txt
|/  
*   4815a7c -- Merge branch '28-request-sandbox-service-to-get-sandboxes-of-the-specific-pool' into 'master'
|\  
| * 4a16740 -- Resolve "Request sandbox service to get sandboxes of the specific pool"
|/  
*   cce4aaf -- Merge branch '27-bump-version-of-sentinel' into 'master'
|\  
| * b3c4bda -- Bump version of Sentinel
|/  
* c136766 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c2ead21 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4514b7d -- Merge branch '26-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * cc0884e -- Update gitlab CI
|/  
* 40e0631 -- Update project package.json version based on GitLab tag. Done by CI
*   734bc27 -- Merge branch '25-update-to-angular-12' into 'master'
|\  
| * 2bd66f7 -- Resolve "Update to Angular 12"
|/  
* 65e6995 -- Update project package.json version based on GitLab tag. Done by CI
*   bfe77cb -- Merge branch '24-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 6e79f96 -- peerDependency update
|/  
* 03cf200 -- Update project package.json version based on GitLab tag. Done by CI
*   aea690e -- Merge branch '23-update-to-angular-11' into 'master'
|\  
| * 9a8534e -- Update to Angular 11 and dependency update
|/  
*   c3728ff -- Merge branch '22-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c772fa8 -- recreate package lock
|/  
* eaa1ab3 -- Update project package.json version based on GitLab tag. Done by CI
*   a4385ca -- Merge branch '21-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * fea31b5 -- Rename the package scope and update dependencies
|/  
*   41ee0fd -- Merge branch '20-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 5e7d5ff -- Migrate to eslint, fix eslint warnings
|/  
* 00d45b4 -- Update project package.json version based on GitLab tag. Done by CI
*   b97b174 -- Merge branch '19-update-dependencies-to-new-format' into 'master'
|\  
| * d3cfdf2 -- Update dependencies to new format
|/  
* 2df711d -- Update project package.json version based on GitLab tag. Done by CI
*   dfb83e4 -- Merge branch '18-rename-to-kypo-sandbox-api' into 'master'
|\  
| * 6c844d2 -- Rename package
|/  
* 71d4359 -- Update project package.json version based on GitLab tag. Done by CI
*   4e3408e -- Merge branch '17-remove-rxjs-deep-imports' into 'master'
|\  
| * 1111f4b -- Removed rxjs deep imports
|/  
* 5e4789d -- Update project package.json version based on GitLab tag. Done by CI
*   15d7cc0 -- Merge branch '16-add-methods-to-obtain-virtual-images' into 'master'
|\  
| * e30ea04 -- Resolve "Add methods to obtain virtual images"
|/  
* a69bbe2 -- Update project package.json version based on GitLab tag. Done by CI
*   9048519 -- Merge branch '13-add-methods-to-obtain-user-and-management-ssh-access' into 'master'
|\  
| * 56ff865 -- Resolve "Add methods to obtain user and management ssh access"
|/  
* 9b30105 -- Update project package.json version based on GitLab tag. Done by CI
*   be2f95b -- Merge branch '15-add-methods-to-obtain-quota-limits' into 'master'
|\  
| * d004689 -- Resolve "Add methods to obtain quota limits"
|/  
*   20183a9 -- Merge branch '12-use-cypress-image-in-ci' into 'master'
|\  
| * c228a96 -- Resolve "Use cypress image in CI"
|/  
* 99b027e -- Update project package.json version based on GitLab tag. Done by CI
*   9e9c7d7 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * a6c429e -- Remove deep imports to rxjs
|/  
* f24596e -- Update project package.json version based on GitLab tag. Done by CI
*   fa12463 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * b7ebedc -- Remove deep imports to rxjs
|/  
* dc986cb -- Update project package.json version based on GitLab tag. Done by CI
*   ca046ec -- Merge branch '10-refactor-to-use-sentinel-common' into 'master'
|\  
| * 2a4861e -- Replace kypo-common deps with @sentinel/common
|/  
* 89065e4 -- Update project package.json version based on GitLab tag. Done by CI
*   8024efc -- Merge branch '4-update-endpoints-of-stages' into 'master'
|\  
| * 252a3d8 -- Resolve "Update endpoints of stages"
|/  
* 184615f -- Update project package.json version based on GitLab tag. Done by CI
*   b523d81 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 12272d2 -- Resolve "Update to Angular 10"
|/  
*   829dceb -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 7b0b951 -- Update .gitlab-ci.yml
|/  
* a4e5d6b -- Merge branch '7-add-no-unused-vars-compiler-rule' into 'master'
* c322ec6 -- Add compiler rule to throw errors on unused locals and parameters
### 12.0.3 Add stage to allocation units
* 299e3d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   11b196f -- Merge branch '29-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 8fb2c17 -- Resolve "Add stage state to allocation unit"
|/  
*   a7fd08e -- Merge branch '31-add-license-file' into 'master'
|\  
| * 5669499 -- Add license file
|/  
*   2934609 -- Merge branch '30-add-node-modules-to-gitignore' into 'master'
|\  
| * 364c137 -- Add node_module to gitignore
|/  
* 5ce6c95 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 383e13f -- [CI/CD] Update packages.json version based on GitLab tag.
*   fc834af -- Merge branch '445537-master-patch-86314' into 'master'
|\  
| * 75eb6a1 -- Update VERSION.txt
|/  
*   4815a7c -- Merge branch '28-request-sandbox-service-to-get-sandboxes-of-the-specific-pool' into 'master'
|\  
| * 4a16740 -- Resolve "Request sandbox service to get sandboxes of the specific pool"
|/  
*   cce4aaf -- Merge branch '27-bump-version-of-sentinel' into 'master'
|\  
| * b3c4bda -- Bump version of Sentinel
|/  
* c136766 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c2ead21 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4514b7d -- Merge branch '26-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * cc0884e -- Update gitlab CI
|/  
* 40e0631 -- Update project package.json version based on GitLab tag. Done by CI
*   734bc27 -- Merge branch '25-update-to-angular-12' into 'master'
|\  
| * 2bd66f7 -- Resolve "Update to Angular 12"
|/  
* 65e6995 -- Update project package.json version based on GitLab tag. Done by CI
*   bfe77cb -- Merge branch '24-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 6e79f96 -- peerDependency update
|/  
* 03cf200 -- Update project package.json version based on GitLab tag. Done by CI
*   aea690e -- Merge branch '23-update-to-angular-11' into 'master'
|\  
| * 9a8534e -- Update to Angular 11 and dependency update
|/  
*   c3728ff -- Merge branch '22-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c772fa8 -- recreate package lock
|/  
* eaa1ab3 -- Update project package.json version based on GitLab tag. Done by CI
*   a4385ca -- Merge branch '21-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * fea31b5 -- Rename the package scope and update dependencies
|/  
*   41ee0fd -- Merge branch '20-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 5e7d5ff -- Migrate to eslint, fix eslint warnings
|/  
* 00d45b4 -- Update project package.json version based on GitLab tag. Done by CI
*   b97b174 -- Merge branch '19-update-dependencies-to-new-format' into 'master'
|\  
| * d3cfdf2 -- Update dependencies to new format
|/  
* 2df711d -- Update project package.json version based on GitLab tag. Done by CI
*   dfb83e4 -- Merge branch '18-rename-to-kypo-sandbox-api' into 'master'
|\  
| * 6c844d2 -- Rename package
|/  
* 71d4359 -- Update project package.json version based on GitLab tag. Done by CI
*   4e3408e -- Merge branch '17-remove-rxjs-deep-imports' into 'master'
|\  
| * 1111f4b -- Removed rxjs deep imports
|/  
* 5e4789d -- Update project package.json version based on GitLab tag. Done by CI
*   15d7cc0 -- Merge branch '16-add-methods-to-obtain-virtual-images' into 'master'
|\  
| * e30ea04 -- Resolve "Add methods to obtain virtual images"
|/  
* a69bbe2 -- Update project package.json version based on GitLab tag. Done by CI
*   9048519 -- Merge branch '13-add-methods-to-obtain-user-and-management-ssh-access' into 'master'
|\  
| * 56ff865 -- Resolve "Add methods to obtain user and management ssh access"
|/  
* 9b30105 -- Update project package.json version based on GitLab tag. Done by CI
*   be2f95b -- Merge branch '15-add-methods-to-obtain-quota-limits' into 'master'
|\  
| * d004689 -- Resolve "Add methods to obtain quota limits"
|/  
*   20183a9 -- Merge branch '12-use-cypress-image-in-ci' into 'master'
|\  
| * c228a96 -- Resolve "Use cypress image in CI"
|/  
* 99b027e -- Update project package.json version based on GitLab tag. Done by CI
*   9e9c7d7 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * a6c429e -- Remove deep imports to rxjs
|/  
* f24596e -- Update project package.json version based on GitLab tag. Done by CI
*   fa12463 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * b7ebedc -- Remove deep imports to rxjs
|/  
* dc986cb -- Update project package.json version based on GitLab tag. Done by CI
*   ca046ec -- Merge branch '10-refactor-to-use-sentinel-common' into 'master'
|\  
| * 2a4861e -- Replace kypo-common deps with @sentinel/common
|/  
* 89065e4 -- Update project package.json version based on GitLab tag. Done by CI
*   8024efc -- Merge branch '4-update-endpoints-of-stages' into 'master'
|\  
| * 252a3d8 -- Resolve "Update endpoints of stages"
|/  
* 184615f -- Update project package.json version based on GitLab tag. Done by CI
*   b523d81 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 12272d2 -- Resolve "Update to Angular 10"
|/  
*   829dceb -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 7b0b951 -- Update .gitlab-ci.yml
|/  
* a4e5d6b -- Merge branch '7-add-no-unused-vars-compiler-rule' into 'master'
* c322ec6 -- Add compiler rule to throw errors on unused locals and parameters
### 12.0.2 New version of Sentinel, added method to request sandboxes of the specific pool.
* 383e13f -- [CI/CD] Update packages.json version based on GitLab tag.
*   fc834af -- Merge branch '445537-master-patch-86314' into 'master'
|\  
| * 75eb6a1 -- Update VERSION.txt
|/  
*   4815a7c -- Merge branch '28-request-sandbox-service-to-get-sandboxes-of-the-specific-pool' into 'master'
|\  
| * 4a16740 -- Resolve "Request sandbox service to get sandboxes of the specific pool"
|/  
*   cce4aaf -- Merge branch '27-bump-version-of-sentinel' into 'master'
|\  
| * b3c4bda -- Bump version of Sentinel
|/  
* c136766 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c2ead21 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4514b7d -- Merge branch '26-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * cc0884e -- Update gitlab CI
|/  
* 40e0631 -- Update project package.json version based on GitLab tag. Done by CI
*   734bc27 -- Merge branch '25-update-to-angular-12' into 'master'
|\  
| * 2bd66f7 -- Resolve "Update to Angular 12"
|/  
* 65e6995 -- Update project package.json version based on GitLab tag. Done by CI
*   bfe77cb -- Merge branch '24-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 6e79f96 -- peerDependency update
|/  
* 03cf200 -- Update project package.json version based on GitLab tag. Done by CI
*   aea690e -- Merge branch '23-update-to-angular-11' into 'master'
|\  
| * 9a8534e -- Update to Angular 11 and dependency update
|/  
*   c3728ff -- Merge branch '22-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c772fa8 -- recreate package lock
|/  
* eaa1ab3 -- Update project package.json version based on GitLab tag. Done by CI
*   a4385ca -- Merge branch '21-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * fea31b5 -- Rename the package scope and update dependencies
|/  
*   41ee0fd -- Merge branch '20-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 5e7d5ff -- Migrate to eslint, fix eslint warnings
|/  
* 00d45b4 -- Update project package.json version based on GitLab tag. Done by CI
*   b97b174 -- Merge branch '19-update-dependencies-to-new-format' into 'master'
|\  
| * d3cfdf2 -- Update dependencies to new format
|/  
* 2df711d -- Update project package.json version based on GitLab tag. Done by CI
*   dfb83e4 -- Merge branch '18-rename-to-kypo-sandbox-api' into 'master'
|\  
| * 6c844d2 -- Rename package
|/  
* 71d4359 -- Update project package.json version based on GitLab tag. Done by CI
*   4e3408e -- Merge branch '17-remove-rxjs-deep-imports' into 'master'
|\  
| * 1111f4b -- Removed rxjs deep imports
|/  
* 5e4789d -- Update project package.json version based on GitLab tag. Done by CI
*   15d7cc0 -- Merge branch '16-add-methods-to-obtain-virtual-images' into 'master'
|\  
| * e30ea04 -- Resolve "Add methods to obtain virtual images"
|/  
* a69bbe2 -- Update project package.json version based on GitLab tag. Done by CI
*   9048519 -- Merge branch '13-add-methods-to-obtain-user-and-management-ssh-access' into 'master'
|\  
| * 56ff865 -- Resolve "Add methods to obtain user and management ssh access"
|/  
* 9b30105 -- Update project package.json version based on GitLab tag. Done by CI
*   be2f95b -- Merge branch '15-add-methods-to-obtain-quota-limits' into 'master'
|\  
| * d004689 -- Resolve "Add methods to obtain quota limits"
|/  
*   20183a9 -- Merge branch '12-use-cypress-image-in-ci' into 'master'
|\  
| * c228a96 -- Resolve "Use cypress image in CI"
|/  
* 99b027e -- Update project package.json version based on GitLab tag. Done by CI
*   9e9c7d7 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * a6c429e -- Remove deep imports to rxjs
|/  
* f24596e -- Update project package.json version based on GitLab tag. Done by CI
*   fa12463 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * b7ebedc -- Remove deep imports to rxjs
|/  
* dc986cb -- Update project package.json version based on GitLab tag. Done by CI
*   ca046ec -- Merge branch '10-refactor-to-use-sentinel-common' into 'master'
|\  
| * 2a4861e -- Replace kypo-common deps with @sentinel/common
|/  
* 89065e4 -- Update project package.json version based on GitLab tag. Done by CI
*   8024efc -- Merge branch '4-update-endpoints-of-stages' into 'master'
|\  
| * 252a3d8 -- Resolve "Update endpoints of stages"
|/  
* 184615f -- Update project package.json version based on GitLab tag. Done by CI
*   b523d81 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 12272d2 -- Resolve "Update to Angular 10"
|/  
*   829dceb -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 7b0b951 -- Update .gitlab-ci.yml
|/  
* a4e5d6b -- Merge branch '7-add-no-unused-vars-compiler-rule' into 'master'
* c322ec6 -- Add compiler rule to throw errors on unused locals and parameters
### 12.0.1 Update gitlab CI
* c2ead21 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4514b7d -- Merge branch '26-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * cc0884e -- Update gitlab CI
|/  
* 40e0631 -- Update project package.json version based on GitLab tag. Done by CI
*   734bc27 -- Merge branch '25-update-to-angular-12' into 'master'
|\  
| * 2bd66f7 -- Resolve "Update to Angular 12"
|/  
* 65e6995 -- Update project package.json version based on GitLab tag. Done by CI
*   bfe77cb -- Merge branch '24-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 6e79f96 -- peerDependency update
|/  
* 03cf200 -- Update project package.json version based on GitLab tag. Done by CI
*   aea690e -- Merge branch '23-update-to-angular-11' into 'master'
|\  
| * 9a8534e -- Update to Angular 11 and dependency update
|/  
*   c3728ff -- Merge branch '22-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c772fa8 -- recreate package lock
|/  
* eaa1ab3 -- Update project package.json version based on GitLab tag. Done by CI
*   a4385ca -- Merge branch '21-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * fea31b5 -- Rename the package scope and update dependencies
|/  
*   41ee0fd -- Merge branch '20-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 5e7d5ff -- Migrate to eslint, fix eslint warnings
|/  
* 00d45b4 -- Update project package.json version based on GitLab tag. Done by CI
*   b97b174 -- Merge branch '19-update-dependencies-to-new-format' into 'master'
|\  
| * d3cfdf2 -- Update dependencies to new format
|/  
* 2df711d -- Update project package.json version based on GitLab tag. Done by CI
*   dfb83e4 -- Merge branch '18-rename-to-kypo-sandbox-api' into 'master'
|\  
| * 6c844d2 -- Rename package
|/  
* 71d4359 -- Update project package.json version based on GitLab tag. Done by CI
*   4e3408e -- Merge branch '17-remove-rxjs-deep-imports' into 'master'
|\  
| * 1111f4b -- Removed rxjs deep imports
|/  
* 5e4789d -- Update project package.json version based on GitLab tag. Done by CI
*   15d7cc0 -- Merge branch '16-add-methods-to-obtain-virtual-images' into 'master'
|\  
| * e30ea04 -- Resolve "Add methods to obtain virtual images"
|/  
* a69bbe2 -- Update project package.json version based on GitLab tag. Done by CI
*   9048519 -- Merge branch '13-add-methods-to-obtain-user-and-management-ssh-access' into 'master'
|\  
| * 56ff865 -- Resolve "Add methods to obtain user and management ssh access"
|/  
* 9b30105 -- Update project package.json version based on GitLab tag. Done by CI
*   be2f95b -- Merge branch '15-add-methods-to-obtain-quota-limits' into 'master'
|\  
| * d004689 -- Resolve "Add methods to obtain quota limits"
|/  
*   20183a9 -- Merge branch '12-use-cypress-image-in-ci' into 'master'
|\  
| * c228a96 -- Resolve "Use cypress image in CI"
|/  
* 99b027e -- Update project package.json version based on GitLab tag. Done by CI
*   9e9c7d7 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * a6c429e -- Remove deep imports to rxjs
|/  
* f24596e -- Update project package.json version based on GitLab tag. Done by CI
*   fa12463 -- Merge branch '11-remove-deep-import-to-rjxs' into 'master'
|\  
| * b7ebedc -- Remove deep imports to rxjs
|/  
* dc986cb -- Update project package.json version based on GitLab tag. Done by CI
*   ca046ec -- Merge branch '10-refactor-to-use-sentinel-common' into 'master'
|\  
| * 2a4861e -- Replace kypo-common deps with @sentinel/common
|/  
* 89065e4 -- Update project package.json version based on GitLab tag. Done by CI
*   8024efc -- Merge branch '4-update-endpoints-of-stages' into 'master'
|\  
| * 252a3d8 -- Resolve "Update endpoints of stages"
|/  
* 184615f -- Update project package.json version based on GitLab tag. Done by CI
*   b523d81 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 12272d2 -- Resolve "Update to Angular 10"
|/  
*   829dceb -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 7b0b951 -- Update .gitlab-ci.yml
|/  
* a4e5d6b -- Merge branch '7-add-no-unused-vars-compiler-rule' into 'master'
* c322ec6 -- Add compiler rule to throw errors on unused locals and parameters
